## Current releases

### Osinfo database

The Osinfo database is updated frequently (whenever there is a relevant set
of new content) and so individual releases are not listed here. Please consult
the [download site](https://releases.pagure.org/libosinfo/) directly for
release archives, named with the filename pattern ``osinfodb-$DATE.tar.xz``.

### Osinfo database tools

#### [osinfo-db-tools 1.2.0](https://releases.pagure.org/libosinfo/osinfo-db-tools-1.2.0.tar.gz) [(gpg)](https://releases.pagure.org/libosinfo/osinfo-db-tools-1.2.0.tar.gz.asc) June 20th, 2018

Changes in this release include

* Don't expand XML entities
* Fix build with CLang
* Don't hardcode perl interpretor path
* Libosinfo library

#### [libosinfo 1.2.0](https://releases.pagure.org/libosinfo/libosinfo-1.2.0.tar.gz) [(gpg)](https://releases.pagure.org/libosinfo/libosinfo-1.2.0.tar.gz.asc) Wednesday June 20th, 2018

Changes in this release include:

* Test suite requires osinfo-db >= 20180612
* Replace use of libsoup with libcurl in test suite
* Replace use of check library with GLib tester in test suite
* Increase timeout for media URIs test
* Drop const parameter annotations which break Rust generator
* Improve error message with ISO test fails
* Turn off -Wfunction-cast-type warning from gcc 8
* Remove calls to g_log_set_always_fatal in tests
* Load database files in alphabetical order for predictable sorting
* Fix leak of list when loading files
* Fix build of tools with clang 6
* Add missing "global" annotation on some symbol versions
* Historical releases

All source archives for the previous releases are available for download at
the [release hosting site](https://releases.pagure.org/libosinfo/)

Previous release notes are available at in the
[NEWS file](https://gitlab.com/libosinfo/libosinfo/blob/master/NEWS).

### Deployment procedure

#### osinfo-db-tools

The osinfo-db-tools package is a pre-requisite of osinfo-db and libosinfo,
so should be built and installed first. It provides a small set of tools
for managing the installation, bundling and distribution of database files

```
$ tar zxvf osinfo-db-tools-$VERSION.tar.gz
$ cd osinfo-db-tools-$VERSION
$ ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var
$ make
$ sudo make install
```

#### osinfo-db

The osinfo-db package does not contain any code, nor does it require building,
as it is purely XML database files. Its contents merely have to be unpacked in
the correct location, using the osinfo-db-import tool. Operating system vendors
packaging it, should use the --system argument to install in the primary
database location.

```
$ sudo osinfo-db-import --system osinfo-db-$VERSION.tar.xz
```

Local system administrators wishing to provide updated content for a host
without conflicting with the OS vendor supplied packages should use the
``--local`` argument to install it in ``/etc``

```
$ sudo osinfo-db-import --local osinfo-db-$VERSION.tar.xz
```

Unprivileged users wishing to provide their account with updated content
should use the ``--user`` argument to install it

```
$ osinfo-db-import --user osinfo-db-$VERSION.tar.xz
```

#### libosinfo

The libosinfo package provides the C library for querying the database, with
ability to use it from non-C languages via GObject Introspection

```
$ tar zxvf libosinfo-$VERSION.tar.gz
$ cd libosinfo-$VERSION
$ ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var
$ make
$ sudo make install
```