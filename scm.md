The libosinfo project uses GIT for managing source code, with the primary
hosting provided by [gitlab](https://gitlab.com).

Currently there are three distinct repositories in use

* [osinfo-db-tools](https://gitlab.com/libosinfo/osinfo-db-tools): a set of command line tools for managing the database files & database archives
* [osinfo-db](https://gitlab.com/libosinfo/osinfo-db): the XML data files making up the database
* [libosinfo](https://gitlab.com/libosinfo/libosinfo): the library API and command line tools for querying the database

To get a direct local checkout use

```
  # git clone https://gitlab.com/libosinfo/<repo-name>.git
```
