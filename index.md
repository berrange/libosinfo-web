The libosinfo project comprises three parts

1. A database of metadata about operating systems, hypervisors, virtual
   hardware and more
2. A GObject based library API for querying information from the database
3. Command line tools for querying & extracting information from the database

The goal of libosinfo is to provide a single place containing all the
information about an operating system that is required in order to provision
and manage it in a virtualized environment. It allows applications to

- Query *what hardware* is supportable by an operating system
- Query *what hardware* is supportable by a hypervisor
- Determine the *optimal hardware* for running an operating system on a
  hypervisor
- Find *download location* of installable ISO images and Live CDs
- Find the location of online *installation trees*
- Query the minimum/recommended/maximum *CPU/memory/disk* resources for an
  operating system
- *Automatically identify* what operating system an ISO image is for
- Generate scripts suitable for *automating JEOS/Desktop installations*

The library API is written in C, using the GObject library (provided by the
glib2 package). Via the magic of ​GObject Introspection, the API is
automatically available in all dynamic programming languages with bindings
for GObject (JavaScript, Perl, Python, Ruby). Auto-generated bindings for
Vala are also provided

The library and database are licensed under the terms of the GNU LGPL
version 2 or later.
